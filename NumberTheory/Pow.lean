--import NumberTheory.Div
--import NumberTheory.Pow.Square

namespace Pow
theorem pow_one (b : Nat) : b^1 = b := by 
  calc
    b^1 = b := by rw [Nat.pow_succ, Nat.pow_zero, Nat.one_mul]

theorem pow_two (b : Nat) : b^2 = b * b := by
  calc
    b^2 = b * b := by rw [Nat.pow_succ, pow_one]

theorem pow_three (b : Nat) : b^3 = b * b * b := by
  calc
    b^3 = b * b * b := by rw [Nat.pow_succ, pow_two]
  
theorem pow_four (b : Nat) : b^4 = b * b * b * b := by
  calc
    b^4 = b * b * b * b := by rw [Nat.pow_succ, pow_three]

theorem power_times_power (b n m : Nat) : b^n * b^m = b^(n + m) := by
  induction m with
  | zero    => calc
      b^n * b^0 = b^n := by rw[Nat.pow_zero, Nat.mul_one]
      _ = b^(n + 0) := by rw[Nat.add_zero]
  | succ m' h => calc
      b^n * b^(Nat.succ m') = (b^n * b^m') * b := by rw[Nat.pow_succ, Nat.mul_assoc]
      _ = b^(n + m') * b := by rw[h]
      _ = b^(Nat.succ (n + m')) := by rw[<-Nat.pow_succ]
      _ = b^(n + m' + 1) := rfl 

theorem power_divided_by_power (b : Nat) {n m : Nat} : m ≤ n → b^n / b^m = b^(n - m) := by
  induction m with 
  | zero    =>
      intros
      calc
        b^n / b^0 = b^n := by rw [Nat.pow_zero, Div.div_one]
  | succ m' h₁ =>
      intro (h₂ : Nat.succ m' ≤ n)
      have : b^n = b^(Nat.succ m') * (b^n / b^(Nat.succ m')) + b^n % b^(Nat.succ m') := 
        Eq.symm (Nat.div_add_mod (b^n) (b^(Nat.succ m')))
      sorry

theorem power_of_power (b n m : Nat) : (b^n)^m = b^(n * m) := by
  induction m with
  | zero      => calc
      (b^n)^0 
        = 1 := by rw [Nat.pow_zero]
      _ = b^0 := by rw [←Nat.pow_zero]
      _ = b^(n * 0) := by rw [Nat.mul_zero]
  | succ m' h => calc
      (b^n)^(Nat.succ m')
        = (b^n)^m' * b^n := by rw [Nat.pow_succ]
      _ = b^(n * m') * b^n := by rw [h]
      _ = b^(n * m' + n) := by rw [power_times_power]
      _ = b^(n * (Nat.succ m')) := by rw [Nat.mul_succ]

end Pow