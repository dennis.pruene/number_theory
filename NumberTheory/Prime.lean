import NumberTheory.Div
open Div


namespace Prime

-- A number p is prime iff it is at least 2 (to avoid 1 being considered prime)
-- and for all divisors d above 1, p is not divisible by d -- or equivalently --
-- there exists some rest class d > r > 0 such that p = d*k + r
def prime (p : Nat) : Prop := p ≥ 2 ∧ ∀ d : {x : Nat // x ≥ 2}, ∃ r : {x : Nat // x > 0}, mod d r p

def Prime : Type := {p : Nat // prime p}

def composite (n : Nat) : Prop := ∃ a b : {x : Nat // x ≥ 2}, n = a * b

def coprime (n m : Nat) : Prop := ∀ d : {x : Nat // x ≥ 2}, ∃ r : {x : Nat // x > 0}, mod d r n ∨ mod d r m
