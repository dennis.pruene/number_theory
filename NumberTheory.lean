def divisible (d n : Nat) : Prop := ∃ k, n = d * k 

theorem gcd_decreasing_lemma (a b : Nat) : a % (b + 1) < b + 1 := Nat.mod_lt a (Nat.succ_pos b)

def gcd (a b : Nat) : Nat :=
  loop (max a b) (min a b)
where
  loop (a : Nat) : Nat → Nat
  | 0   => a
  | b+1 => 
    have : a % (b + 1) < b + 1 := gcd_decreasing_lemma a b
    loop (b + 1) (a % (b + 1))

theorem mod_eq_diff (a b : Nat) : ∃ k, a % b = a - b * k ∧ b * k ≤ a := by
  induction a, b using Nat.mod.inductionOn with
  | ind a b h ih =>
    let ⟨k, ih₁, ih₂⟩ := ih
    apply Exists.intro (k + 1)
    apply And.intro
    case left =>
      simp [←Nat.sub_add_eq, Nat.add_comm b (b * k), ←Nat.mul_succ, ←Nat.mod_eq_sub_mod h.right] at ih₁
      assumption
    case right =>
      have ih₂ := Nat.add_le_of_le_sub h.right ih₂
      simp [←Nat.mul_succ] at ih₂
      assumption
  | base a b h =>
    match b with
    | 0 =>
      apply Exists.intro 0
      simp [Nat.mod_zero]
    | b+1 =>
      have h : b + 1 > a := by
        apply Nat.gt_of_not_le
        intro hble
        have : 0 < b + 1 ∧ b + 1 ≤ a := ⟨Nat.succ_pos b, hble⟩
        contradiction
      apply Exists.intro 0
      simp [Nat.mod_eq_of_lt h]

theorem gcd_is_common_divisor (a b : Nat) : divisible (gcd a b) a ∧ divisible (gcd a b) b := by
  let rec aux (a b : Nat) : divisible (gcd.loop a b) a ∧ divisible (gcd.loop a b) b := by
    match b with
    | 0   => 
      simp [gcd.loop]
      exact ⟨⟨1, by simp⟩, ⟨0, rfl⟩⟩
    | b+1 =>
      simp [gcd.loop]
      have : a % (b + 1) < b + 1 := gcd_decreasing_lemma a b
      have ⟨⟨k₁, h₁⟩, ⟨k₂, h₂⟩⟩ := aux (b + 1) (a % (b + 1))
      apply And.intro
      case right =>
        exact ⟨k₁, h₁⟩
      case left =>
        generalize h : gcd.loop (b + 1) (a % (b + 1)) = d
        simp [h] at h₁ h₂
        have ⟨k₃, h₃, h₄⟩ : ∃ k, a % (b + 1) = a - (b + 1) * k ∧ (b + 1) * k ≤ a := mod_eq_diff a (b + 1)
        have h₃ : a = a % (b + 1) + (b + 1) * k₃ := Nat.eq_add_of_sub_eq h₄ (Eq.symm h₃)
        rw [h₂, h₁, Nat.mul_assoc, ←Nat.left_distrib d k₂ (k₁ * k₃)] at h₃
        exact ⟨k₂ + k₁ * k₃, h₃⟩ 
  simp [gcd, Nat.max_def, Nat.min_def]
  split
  case inl =>
    have := aux b a
    exact ⟨this.right, this.left⟩
  case inr =>
    exact aux a b 

theorem gcd_is_greatest (a b d : Nat) : divisible d a → divisible d b → divisible d (gcd a b) := by
  let rec aux (a b d : Nat) : divisible d a → divisible d b → divisible d (gcd.loop a b) := by
    intro ⟨k, haeq⟩ ⟨l, hbeq⟩ 
    match b with
    | 0 =>
      simp [gcd.loop]
      show divisible d a
      exact ⟨k, haeq⟩
    | b+1 =>
      simp [gcd.loop]
      have : a % (b + 1) < b + 1 := gcd_decreasing_lemma a b
      suffices h : divisible d (a % (b + 1)) from aux (b + 1) (a % (b + 1)) d ⟨l, hbeq⟩ h
      have ⟨m, h, _⟩ := mod_eq_diff a (b + 1)
      rw [h, haeq, hbeq, Nat.mul_assoc, ←Nat.mul_sub_left_distrib d k (l * m)]
      exact ⟨k - l * m, rfl⟩
  intros hadiv hbdiv
  simp [gcd, Nat.min_def, Nat.max_def]
  split
  · exact aux b a d hbdiv hadiv
  · exact aux a b d hadiv hbdiv  
